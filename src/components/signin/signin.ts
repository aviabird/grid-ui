import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { UserService } from '../../shared/services/userservice';
import { SharedState } from '../../shared/state/sharedstate';

@inject(UserService, SharedState, Router)

export class Signin {

  email = '';
  password = '';
  errors = null;

  constructor(private userService: UserService, private sharedState: SharedState, private router: Router) {
    this.userService = userService;
    this.sharedState = sharedState;
    this.router = router;

  }

  submit() {
    
    this.errors = null;

    const credentials = {
      email: this.email,
      password: this.password
    };
    this.userService.attemptAuth(credentials)
      .then(data => this.router.navigateToRoute('dashboard'))
      .catch(promise => {
        promise.then(err => {
          debugger;
          this.errors = err.errors })
      });

  }
}
