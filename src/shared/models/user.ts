export class User {
  
  jwt: string;
  email: string;
  name: string;
  role: '';
  id: '';
  constructor() {
    this.email = '';
    this.jwt = '';
    this.role = '';
    this.name = '';
    this.id = '';
  }
  
}
