import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { CommunityService } from '../../shared/services/communityservice';
import { UserService } from '../../shared/services/userservice';
import { JwtService } from '../../shared/services/jwtservice';
import { ProfileService } from '../../shared/services/profileservice';
import { InvitationService } from '../../shared/services/invitationservice';

@inject(UserService, CommunityService, JwtService, InvitationService, ProfileService, Router)

export class CommunityCreate {
    errors = '';
    name = '';
    community = [];
    message = '';
    users = [];
    profiles = [];
    role = null;
    isCommunityManager: boolean;

    constructor(private userService: UserService, private communityService: CommunityService, private jwtService: JwtService, private invitationService: InvitationService, private profileService: ProfileService, private router: Router) {
        this.userService = userService;
        this.communityService = communityService;
        this.profileService = profileService;
        this.jwtService = jwtService;
        this.invitationService = invitationService;
        this.router = router;

        this.role = this.jwtService.getRole();
        this.isCommunityManager = (this.role == "Community Manager");
    }

    addUserToCommunity(params) {
        this.profileService.register(params)
            .then(data => {
                this.router.navigateToRoute('communityview', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });
    }

    sendInvite(params) {
        this.invitationService.register(params)
        .then(data => {
            this.router.navigateToRoute('communityview', data);
        })
        .catch(promise => {
            promise.then(err => { this.errors = err.errors })
        });
    }

    submit(params) {
        this.errors = '';
        const newparams = {
            name: this.name
        };
        this.communityService.register(newparams)
            .then(data => {
                this.message = data.message;
                this.name = '';
                this.router.navigateToRoute('communitymanagement', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}