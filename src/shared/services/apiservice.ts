import { config } from './config';
import { HttpClient, json } from 'aurelia-fetch-client';
import { inject } from 'aurelia-dependency-injection';
import { JwtService } from './jwtservice';
import { status, parseError } from './servicehelper';

@inject(HttpClient, JwtService)
export class ApiService {

  constructor(private http: HttpClient, private jwtService: JwtService) {
    this.http = http;
    this.jwtService = jwtService;
  }

  setHeaders() {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = 'Bearer ' + this.jwtService.getToken()
    }
    this.http.configure(config_ => {
      config_
        .withBaseUrl(config.api_url)
        .withDefaults({
          headers: headersConfig
        })
    });
  }

  get(path, params) {
    this.setHeaders()
    return this.http.fetch(path, {
      method: 'GET'
    })
      .then(status)
      // .catch(parseError)
      .catch((error:any) => {
        parseError(error)
      })
  }

  post(path, body = {}) {
    this.setHeaders()

    return this.http.fetch(path, {
      method: 'POST',
      body: json(body)
    })
      .then(status)
      // .catch(parseError)
      .catch((error:any) => {
        parseError(error)
      })

  }

}
