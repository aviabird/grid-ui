import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { InvitationService } from '../../shared/services/invitationservice';

@inject(JwtService, InvitationService, Router)

export class InvitationList {
    receiver_email = '';
    errors = '';
    sender_id = '';
    community_id = '';
    invitations = [];

    constructor(private jwtService: JwtService, private invitationService: InvitationService, private router: Router) {
        this.invitationService = invitationService;
        this.jwtService = jwtService;
        this.router = router;
        this.activate({});

    }

    activate(params) {
        this.community_id = params.community_id;
        var user_id = this.jwtService.getId();
        if (user_id)
            {
                this.getAllInvitationsBy({user_id: user_id});
            }        
    }

    getAllInvitations(){
        this.invitationService.getinvitations()
        .then(data => {
            this.invitations = data.invitations;
        })
        .catch(promise => {
            promise.then(err => { this.errors = err.errors })
        });
    }

    getAllInvitationsBy(params){
        this.invitationService.getinvitationsby(params)
        .then(data => {
            this.invitations = data.invitations;
        })
        .catch(promise => {
            promise.then(err => { this.errors = err.errors })
        });
    }
}