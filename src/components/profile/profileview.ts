import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { ProfileService } from '../../shared/services/profileservice';

@inject(ProfileService, Router)

export class ProfileView {
    user_id = '';
    community_id = '';
    errors = '';

    constructor(private profileService: ProfileService, private router: Router) {
        this.profileService = profileService;
        this.router = router;

    }

    activate(params) {
        this.submit(params);
    }

    submit(params) {
        this.errors = '';

        this.profileService.register(params)
            .then(data => {
                this.router.navigateToRoute('profile', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}