export class JwtService {
  
  getToken() {
    return window.localStorage['jwtToken'];
  }
  
  getName() {
    return window.localStorage['jwtName'];
  }

  getRole() {
    return window.localStorage['jwtRole'];
  }

  getEmail() {
    return window.localStorage['jwtEmail'];
  }

  getId() {
    return window.localStorage['jwtId'];
  }

  saveToken(token) {
    window.localStorage['jwtToken'] = token.jwt;
    window.localStorage['jwtName'] = token.name;
    window.localStorage['jwtEmail'] = token.email;
    window.localStorage['jwtRole'] = token.role;
    window.localStorage['jwtId'] = token.id;
  }
  
  destroyToken() {
    window.localStorage.removeItem('jwtToken');
    window.localStorage.removeItem('jwtName');
    window.localStorage.removeItem('jwtEmail');
    window.localStorage.removeItem('jwtRole');
    window.localStorage.removeItem('jwtId');
  }
  
}
