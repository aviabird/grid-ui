import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { CommunityService } from '../../shared/services/communityservice';
import { UserService } from '../../shared/services/userservice';
import { JwtService } from '../../shared/services/jwtservice';
import { ProfileService } from '../../shared/services/profileservice';
import { InvitationService } from '../../shared/services/invitationservice';

@inject(UserService, CommunityService, JwtService, InvitationService, ProfileService, Router)
export class CommunityManagement {
    errors = '';
    community = [];
    users = [];
    profiles = [];
    message = '';
    role = null;
    receiver_email = '';
    sender_id = '';
    community_id = '';
    isCommunityManager: boolean;

    constructor(private userService: UserService, private communityService: CommunityService, private jwtService: JwtService, private invitationService: InvitationService, private profileService: ProfileService, private router: Router) {
        this.userService = userService;
        this.communityService = communityService;
        this.profileService = profileService;
        this.jwtService = jwtService;
        this.invitationService = invitationService;
        this.router = router;

        this.role = this.jwtService.getRole();
        // this.isCommunityManager = (this.role == "6");
        this.isCommunityManager = (this.role == "Community Manager");
    }

    activate(params) {
        this.community_id = params.id;
        this.sender_id = this.jwtService.getId();
        this.communityService.getCommunity({ id: params.id })
            .then(data => {
                this.community = data.community;
            });
    }
}