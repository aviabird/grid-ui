import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { InvitationService } from '../../shared/services/invitationservice';

@inject(JwtService, InvitationService, Router)

export class InvitationView {
    receiver_email = '';
    errors = '';
    sender_id = '';
    community_id = '';
    invitation = [];

    constructor(private jwtService: JwtService, private invitationService: InvitationService, private router: Router) {
        this.invitationService = invitationService;
        this.router = router;
        this.activate({});

    }

    activate(params) {
        this.getInvitation(params);
    }

    getInvitation(params) {
        this.invitationService.getinvitation(params)
            .then(data => {
                this.invitation = data.invitation;
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });
    }
}