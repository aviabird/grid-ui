import { inject } from 'aurelia-dependency-injection';
import { ApiService } from './apiservice';

@inject(ApiService)
export class ProfileService {
    constructor(private apiService: ApiService) {
        this.apiService = apiService;
    }

    register(params) {
        return this.apiService.post('profile', params)
            .then(data => {
                return data;
            });
    }

    getProfiles(params) {
        return this.apiService.get('profile', params)
            .then(data => {
                return data;
            });
    }

    getProfile(params) {
        return this.apiService.get('profile/' + params.id, {})
            .then(data => {
                return data;
            });
    }
}