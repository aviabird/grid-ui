import {
    EventAggregator,
    Subscription
} from 'aurelia-event-aggregator';
import {
    autoinject,
    ComponentAttached,
    PLATFORM
} from 'aurelia-framework';
import {
    RoutableComponentActivate,
    RoutableComponentDeactivate,
    Router,
    RouterConfiguration
} from 'aurelia-router';
import * as NProgress from 'nprogress';
import '../style/index.scss';


const MS_FOR_LOADER_BAR_TO_APPEAR = 50;

@autoinject()
export class App {
    private processingSubscription!: Subscription;
    private completeSubscription!: Subscription;
    private router!: Router;

    private readonly events: EventAggregator;

    constructor(events: EventAggregator) {
        this.events = events;
    }

    configureRouter(config: RouterConfiguration, router: Router): void {
        this.router = router;

        config.title = 'Grid';
        config.mapRoute({
            route: ['', '/'], moduleId: PLATFORM.moduleName('./components/index/index'), name: 'home', title: 'Home'
        }).mapRoute({
            route: 'dashboard', moduleId: PLATFORM.moduleName('./components/dashboard/dashboard'), name: 'dashboard', title: 'Dashboard'
        }).mapRoute({
            route: 'signin', moduleId: PLATFORM.moduleName('./components/signin/signin'), name: 'signin', title: 'Sign In'
        }).mapRoute({
            route: 'signup', moduleId: PLATFORM.moduleName('./components/signup/signup'), name: 'signup', title: 'Sign Up'
        }).mapRoute({
            route: 'community', moduleId: PLATFORM.moduleName('./components/community/community'), name: 'community', title: 'Community'
        }).mapRoute({
            route: 'communityview', moduleId: PLATFORM.moduleName('./components/community/communityview'), name: 'communityview', title: 'Community View'
        }).mapRoute({
            route: 'communitycreate', moduleId: PLATFORM.moduleName('./components/community/communitycreate'), name: 'communitycreate', title: 'Community Create'
        }).mapRoute({
            route: 'communitymanagement', moduleId: PLATFORM.moduleName('./components/community/communitymanagement'), name: 'communitymanagement', title: 'Community Management'
        }).mapRoute({
            route: 'profileview', moduleId: PLATFORM.moduleName('./components/profile/profileview'), name: 'profileview', title: 'Profile View'
        }).mapRoute({
            route: 'invitation', moduleId: PLATFORM.moduleName('./components/invitation/invitation'), name: 'invitation', title: 'Invitation'
        }).mapRoute({
            route: 'invitationview', moduleId: PLATFORM.moduleName('./components/invitation/invitationview'), name: 'invitationview', title: 'Invitation View'
        })
        .mapUnknownRoutes({
            route: 'unknown',
            redirect: ''
        });
    }


    private showLoaderBar = (): void => {
        setTimeout(() => {
            if (this.router.isNavigating) {
                NProgress.start();
            }
        }, MS_FOR_LOADER_BAR_TO_APPEAR);
    };
}

