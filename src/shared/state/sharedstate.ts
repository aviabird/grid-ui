import {User} from '../models/user';

export class SharedState {

  isAuthenticated: boolean;
  currentUser: User;
  constructor() {
    this.currentUser = new User();
    this.isAuthenticated = false;
  }
}
