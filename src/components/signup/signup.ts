import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
// import { ValidationControllerFactory, ValidationRules, ValidationController } from 'aurelia-validation';
import { UserService } from '../../shared/services/userservice';
import { SharedState } from '../../shared/state/sharedstate';

@inject(UserService, SharedState, Router)
export class Signup {
  email = '';
  password = '';
  firstname = '';
  lastname = '';
  passwordconfirmation = '';
  // role = this.userService.getroles({});
  errors = null;
  // controller = null;

  constructor(private userService: UserService, private sharedState: SharedState, private router: Router) {
    this.userService = userService;
    this.sharedState = sharedState;
    this.router = router;
    // this.controller = controllerFactory.createForCurrentScope();

    // ValidationRules
    // .ensure('email').required().email()
    // .ensure('password').required().minLength(8)
    // .ensure('passwordconfirmation').required().minLength(8)
    // .ensure('firstname').required().minLength(3)
    // .ensure('lastname').required().minLength(3)
    // .on(this);
  }

  submit() {
    this.errors = null;
    // console.log(this.controller.validate())
    
    // this.controller.validate()
    //   .then(result => {
    //     if (result.valid) {
          const credentials = {
            email: this.email,
            password: this.password,
            password_confirmation: this.passwordconfirmation,
            first_name: this.firstname,
            last_name: this.lastname
          };
          this.userService.register(credentials)
            .then(data => this.router.navigateToRoute('signin'))
            .catch(promise => {
              promise.then(err => { this.errors = err.errors })
            });
      //   }
      // })

  }
}
