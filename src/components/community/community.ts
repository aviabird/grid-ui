import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { CommunityService } from '../../shared/services/communityservice';

@inject(CommunityService, JwtService, Router)

export class Community {
    name = '';
    errors = '';
    role = '';
    isCommunityManager: boolean;

    constructor(private communityService: CommunityService, private jwtService: JwtService, private router: Router) {
        this.communityService = communityService;
        this.router = router;
        this.jwtService = jwtService;
        this.role = this.jwtService.getRole();
        this.isCommunityManager = (this.role == "Community Manager");

    }

    submit() {
        this.errors = '';
        this.role = this.jwtService.getRole();
        const params = {
            name: this.name
        };

        this.communityService.register(params)
            .then(data => {
                this.router.navigateToRoute('communityview', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}