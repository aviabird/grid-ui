import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { CommunityService } from '../../shared/services/communityservice';

@inject(CommunityService, JwtService, Router)

export class CommunityList {
    errors = '';
    communities = [];
    mycommunities = [];
    role = '';

    constructor(private communityService: CommunityService, private jwtService: JwtService, private router: Router) {
        this.communityService = communityService;
        this.router = router;
        this.jwtService = jwtService;
        this.load();
    }

    load() {
        this.role = this.jwtService.getRole();
        this.communityService.getCommunities({})
            .then(data => {
                this.communities = data.communities;
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

        this.communityService.getCommunityListByUser({ user_id: this.jwtService.getId() })
            .then(data => {
                this.mycommunities = data.communities;
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}