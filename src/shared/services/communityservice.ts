import { inject } from 'aurelia-dependency-injection';
import { ApiService } from './apiservice';

@inject(ApiService)
export class CommunityService {
    constructor(private apiService: ApiService) {
        this.apiService = apiService;
    }

    register(params) {
        return this.apiService.post('community', params)
            .then(data => {
                return data;
            });
    }

    getCommunities(params) {
        return this.apiService.get('community', params)
            .then(data => {
                return data;
            });
    }

    getCommunity(params) {
        return this.apiService.get('community/' + params.id, {})
            .then(data => {
                return data;
            });
    }

    getCommunityListByUser(params) {
        return this.apiService.get('community/user/' + params.user_id, {})
            .then(data => {
                return data;
            });
    }

    getCommunityMemberByCommunity(params) {
        return this.apiService.get('community/user/' + params.community_id, {is_member: true})
            .then(data => {
                return data;
            });
    }

    getNonCommunityMemberByCommunity(params) {
        return this.apiService.get('community/user/' + params.community_id, {is_member: false})
            .then(data => {
                return data;
            });
    }
}