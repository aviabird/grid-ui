import { inject } from 'aurelia-dependency-injection';
import { ApiService } from './apiservice';

@inject(ApiService)
export class InvitationService {
    constructor(private apiService: ApiService) {
        this.apiService = apiService;
    }

    register(params) {
        return this.apiService.post('invitation', params)
            .then(data => {
                return data;
            });
    }

    getinvitations() {
        return this.apiService.get('invitation', {})
            .then(data => {
                return data;
            });
    }

    getinvitationsby(params) {
        return this.apiService.get('invitation/user/' + params.user_id, params)
            .then(data => {
                return data;
            });
    }

    getinvitation(params) {
        return this.apiService.get('invitation/' + params.id, {})
            .then(data => {
                return data;
            });
    }
}