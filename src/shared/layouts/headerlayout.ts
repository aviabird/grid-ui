import { inject } from 'aurelia-dependency-injection';
import { UserService } from '../../shared/services/userservice';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { SharedState } from '../state/sharedstate';

@inject(UserService, SharedState, JwtService, Router)
export class HeaderLayout {
    role = null;
    isCommunityManager: boolean;
    constructor(private userService: UserService, private sharedState: SharedState, private jwtService: JwtService, private router: Router) {
        this.userService = userService;
        this.sharedState = sharedState;
        this.jwtService = jwtService;
        this.router = router;
        this.userService.populate();
        this.role = this.jwtService.getRole();
        this.isCommunityManager = (this.role == "Community Manager");

    }

    logout() {
        this.userService.purgeAuth();
        this.router.navigateToRoute('signin');

    }
}