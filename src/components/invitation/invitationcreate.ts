import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { JwtService } from '../../shared/services/jwtservice';
import { InvitationService } from '../../shared/services/invitationservice';
import {bindable} from 'aurelia-framework';

@inject(JwtService, InvitationService, Router)

export class InvitationCreate {
    receiver_email = '';
    errors = '';
    sender_id = '';
    @bindable community_id;
    invitations = [];
    message = '';

    constructor(private jwtService: JwtService, private invitationService: InvitationService, private router: Router) {
        this.invitationService = invitationService;
        this.router = router;
    }
    
    submit(params) {
        this.errors = '';
        this.sender_id = this.jwtService.getId();
        const newparams = {
            receiver_email: this.receiver_email,
            sender_id: this.sender_id,
            community_id: this.community_id
        };
        this.invitationService.register(newparams)
            .then(data => {
                this.message = data.message;
                this.receiver_email = '';
                this.router.navigateToRoute('communityview', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}