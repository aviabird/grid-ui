import { inject } from 'aurelia-dependency-injection';
import { Router, activationStrategy } from 'aurelia-router';
import { CommunityService } from '../../shared/services/communityservice';
import { UserService } from '../../shared/services/userservice';
import { JwtService } from '../../shared/services/jwtservice';
import { ProfileService } from '../../shared/services/profileservice';
import { InvitationService } from '../../shared/services/invitationservice';

@inject(UserService, CommunityService, JwtService, InvitationService, ProfileService, Router)

export class CommunityView {
    errors = '';
    community = [];
    users = [];
    profiles = [];
    message = '';
    role = null;
    receiver_email = '';
    sender_id = '';
    community_id = '';
    isCommunityManager: boolean;

    constructor(private userService: UserService, private communityService: CommunityService, private jwtService: JwtService, private invitationService: InvitationService, private profileService: ProfileService, private router: Router) {
        this.userService = userService;
        this.communityService = communityService;
        this.profileService = profileService;
        this.jwtService = jwtService;
        this.invitationService = invitationService;
        this.router = router;

        this.role = this.jwtService.getRole();
        // this.isCommunityManager = (this.role == "6");
        this.isCommunityManager = (this.role == "Community Manager");
    }


    activate(params) {
        this.community_id = params.community_id;
        this.sender_id = this.jwtService.getId();
        this.communityService.getCommunity({ id: params.community_id })
            .then(data => {
                this.community = data.community;
                this.community_id = data.community.id;
            });
        if (this.isCommunityManager == true) {
            this.userService.getCommunityUsers(this.community_id, false)
                .then(data => {
                    this.users = data.users;
                });
        }

        this.profileService.getProfiles(params)
            .then(data => {
                this.profiles = data.profiles;
            });
    }

    addUserToCommunity(params) {
        this.profileService.register(params)
            .then(data => {
                this.message = data.message;
                this.router.navigateToRoute('communitylist', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });
    }

    sendInvite(params) {
        this.errors = '';
        const newparams = {
            receiver_email: params.receiver_email,
            sender_id: params.sender_id,
            community_id: params.community_id
        };
        this.invitationService.register(newparams)
            .then(data => {
                this.message = data.message;
                this.receiver_email = '';
                this.users = [];
                this.userService.getCommunityUsers(this.community_id, false)
                .then(data => {
                    this.users = data.users;
                });
                this.router.navigateToRoute('communityview', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });
    }

    submit(params) {
        this.errors = '';
        this.sender_id = this.jwtService.getId();
        const newparams = {
            receiver_email: this.receiver_email,
            sender_id: this.sender_id,
            community_id: this.community_id
        };
        this.invitationService.register(newparams)
            .then(data => {
                this.router.navigateToRoute('communityview', data);
            })
            .catch(promise => {
                promise.then(err => { this.errors = err.errors })
            });

    }
}