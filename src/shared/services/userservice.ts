import {inject} from 'aurelia-dependency-injection';
import {ApiService} from './apiservice';
import {JwtService} from './jwtservice';
import { Router, activationStrategy } from 'aurelia-router';
import {User} from '../models/user';
import {SharedState} from '../state/sharedstate';

@inject(ApiService, JwtService, SharedState, Router)
export class UserService {
  
  constructor(private apiService: ApiService, private jwtService: JwtService, private sharedState: SharedState, private router: Router) {
    this.apiService = apiService;
    this.jwtService = jwtService;
    this.router = router;
    this.sharedState = sharedState;
  }

  populate() {
    var id = this.jwtService.getId()
    if (id) {
      this.apiService.get('user/' + id, {})
        .then(data => this.setUser(data.user))
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
      // this.router.navigateToRoute('signin');
    }
  }
  
  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    this.sharedState.currentUser = new User();
    this.sharedState.isAuthenticated = false;
  }

  setAuth(user) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user);
    this.sharedState.currentUser = user;
    this.sharedState.isAuthenticated = true;
  }

  setUser(user) {
    // Save JWT sent from server in localstorage
    // this.jwtService.saveToken(user);
    this.sharedState.currentUser = user;
    this.sharedState.isAuthenticated = true;
  }

  attemptAuth(credentials) {
    return this.apiService.post('login' , credentials)
      .then(data => {
        this.setAuth(data.user);
        return data;
      });
  }
  
  register(credentials) {
    return this.apiService.post('user' , credentials)
      .then(data => {
        return data;
      });
  }

  getUsers(params) {
    return this.apiService.get('user', {})
    .then(data => {
      return data;
    });
  }

  getCommunityUsers(community_id, is_member) {
    return this.apiService.get('community/' + community_id + '/user?is_member=' + is_member, {})
    .then(data => {
      return data;
    });
  }
  // http://0.0.0.0:4000/api/v1/community/1/user?is_member=false
  getRoles(params) {
    return this.apiService.get('role', params)
    .then(data => {
      return data;
    });
  }
}
